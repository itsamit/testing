package com.directfromchef.services.login;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.directfromchef.dao.login.LoginDAO;
import com.directfromchef.transferobject.LoginTO;
import com.sun.jersey.api.core.InjectParam;



@Service
@Path(value = "/login")
public class LoginServices {
       @InjectParam LoginDAO loginDAO;
	
	@POST
	@Path(value = "/validateUser")
	public  Response validateUser(LoginTO loginTO) {
		System.out.println(loginTO.getUsername());
		System.out.println("service..");
		loginDAO.validateUser();
		return Response.ok(loginTO, MediaType.APPLICATION_JSON).build();
	}
	
	@RequestMapping(value="/springAngularJS11", method=RequestMethod.GET, produces={"application/xml", "application/json"})
    public @ResponseBody Person  getPerson() {      
        Person person = new Person();
        person.setFirstName("Java");
        person.setLastName("Honk");
        return person;
    }
	
	@GET
	@Path(value="/test")
	public Person test(){
		//System.out.println("hhh" +person);
		Person person = new Person();
        person.setFirstName("Java");
        person.setLastName("Honk");
        return person;
	}
	
	@GET
	@Path(value="/test11")
	public @ResponseBody Person test11(@RequestBody  Person person){
		System.out.println("hhh" +person);
		//Person person = new Person();
        person.setFirstName("Java");
        person.setLastName("Honk");
        return person;
	}
	
	@GET
	@Path(value="/test1")
	public @ResponseBody Person test1( Person person){
		System.out.println("hhh" +person);
		//Person person = new Person();
        person.setFirstName("Java");
        person.setLastName("Honk");
        return person;
	}
	
	@GET
	@Path(value="/test2")
	public  Person test2( String person1){
		System.out.println("hhh" +person1);
		Person person = new Person();
        person.setFirstName("Java");
        person.setLastName("Honk");
        return person;
	}
	@GET
	@Path(value = "/validateUser1")
	public String validateUser() {
		// returnTO = new LoginTO();
		System.out.println("servicexc bx..");
		return "kkll";
	}
}
